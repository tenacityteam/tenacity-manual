# The Tenacity Manual

This is the repository containing the source of the Tenacity manual. It is
built with [Sphinx](https://sphinx-doc.org).

The manual can be built as either HTML output (with the Haiku theme enabled), a
PDF, or an EPUB, just to name a few. There are plenty of more formats that
Sphinx can output.

# Building

## Pre-requisites
Make sure you have the following installed:

* Python 3 and PIP
* `sphinx-build`
* Any `texlive` packages
* **Optional on Linux only**: GNU Make

## Windows
To build on Windows, run `make.bat` in a Command Prompt window.

Alternatively, you can run `sphinx-build .\source .\build`.

## Linux
To build on Linux, run `make html` to build an HTML version of the manual. For
a list of all possible options, just run `make`.

Alternatively, you can run `sphinx-build ./source ./build`.

# License
The Tenacity manual is licensed under [CC BY 4.0](LICENSE.md).
