# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Tenacity'
copyright = '2022-2024 Tenacity Community. Licensed under CC-BY 4.0'
author = 'Tenacity Community'
release = '1.4e1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['myst_parser']

# MyST Options
myst_enable_extensions = ['smartquotes', 'linkify']
myst_heading_anchors = 4 # This can be increased as necessary

#templates_path = ['templates']
exclude_patterns = []

locale_dirs = ['locale/']
gettext_compact = False

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'haiku'
html_logo = 'images/tenacity-logo-light-200.png'
html_theme_options = {
    'full_logo' : True
}
#html_static_path = ['static']
