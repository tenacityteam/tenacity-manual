# Preferences

Tenacity's preferences can be accessed under *Edit > Preferences*.

## General Information

Tenacity's entire user profile folder is stored in different directories
depending on your platform:

- On Windows, it is stored in `C:\Users\<user_name>\AppData\Roaming\Tenacity`.
- On Linux, it depends on which version you use:

  - If using the Flatpak, it is stored in
    `~/.var/app/org.tenacityaudio.Tenacity/config`.

  - If using the AppImage or any regular system package, it is stored in
    `~/.config/tenacity`.

Tenacity also supports using portable settings. This can be useful for sharing
the same installation across multiple computers or to test a build. In the same
folder as the installation, create a new folder called "Portable Settings". Any
changes you make to Tenacity will be saved in this directory and will not affect
your regular settings directory.

```{note}
**TODO**: Add Haiku preferences directory.
```

## Migrating from Audacity Preferences

Tenacity does _not_ support migrating from Audacity preferences. We
explicitly dropped the ability to migrate Audacity 1.x preferences to
Tenacity's preferences, a feature inherited from Audacity. However, if you want
a way to migrate your Audacity preferences regardless, you can copy
`audacity.cfg` from your Audacity preferences directory to your Tenacity
preferences directory as `tenacity.cfg`. *Do not copy pluginsettings.cfg,
pluginregistry.cfg, or any other *.cfg file. This will cause issues with your
plugins.*

If you experience any issues or errors after copying Audacity's preferences try
using _Tools > Reset Configuration_. If you continue to experience errors, stop
Tenacity and delete your profile folder.

## Devices

This preference panel contains all options related to configuring devices.

**Interface**

- **Host**: Which audio host Tenacity should use. Options vary by platform, but
  include WASAPI (Windows), CoreAudio (macOS), ALSA (Linux), and JACK
  (cross-platform).

**Playback**

- **Device**: The selected playback device. Can be alternatively quickly
  configured through the Device Toolbar.

**Recording**

- **Device**: The selected recording device. Can be alternatively quickly
  configured through the Device Toolbar.

- **Channels**: How many channels to record. Can be alternatively quickly
  configured through the Device Toolbar.

```{note}
Extra recording channels (i.e., more than 2 channels) are recorded as
separate mono tracks. 2 channels are recorded as a single stereo track, which
can be split later.
```

**Latency**

- **Buffer length**: The suggested latency for the current device. Default is
  512 samples.

- **Latency compensation**: The amount of recording latency Tenacity should
  compensate for. Also measured in milliseconds. See :ref:`tips_and_tricks`.

```{note}
In previous versions of Tenacity, the buffer length is ignored if the host
API is WASAPI. This is due to a (PortAudio?) bug. All other host APIs are
unaffected.
```

## Playback

**Effects Preview**

- **Length**: how long effect previews should be. Default is 6 seconds.

```{note}
Not all effects follow this preference. Effects that support realtime
preview, like the Bass and Treble effect, have their own playback controls
that can be previewed for any length.
```

**Cut Preview**

- **Before cut region**: How long audio should be played before the cutline
  selection. Default is 2 seconds.

- **After cut region**: How long audio should be played after the cutline
  selection. Default is 1 second.

**Seek Time**

- **Short period**: How long to seek when regular seeking. Default is 1 second.

- **Long period**: How to long see when long seeking. Default is 15 seconds.

**Options**

- **Vari-Speed Play**: Enables playback at variable speeds when playback is
  started through the Playback Speed toolbar. Default is on.

- **Micro-fades**: Enables micro-fading when starting and stopping audio for
  smoother transitions.

- **Always scrub unpinned**: Ignores the pinned playhead when scrubbing. Enabled
  by default.

## Recording

**Options**

- **Play other tracks while recording (overdub)**: Enables playback of other
  tracks while recording. Default is on.

- **Software playthrough of input**: Enables playback of your current input
  device. Disabled by default.

- **Record on a new track**: Records on a new track. Disabled by default.

- **Detect dropouts**: Enables detection of audio dropouts. This creates a new
  label track with various labels at points in time where audio has dropped
  out. Enabled by default.

**Sound activated recording**

Sound activated recording automatically starts and stops recording once the
input level reaches the activation volume. The activation volume is configurable
between -60 dB and 0 dB.

To start sound activated recording, click the 'Record' button. Recording will
automatically start when the input volume is over the activation volume and will
automatically stop recording when the input volume is under the activation
volume.

**Name newly recorded tracks**

This section allows you to customize the name of newly recorded tracks. You can
include a custom name, a track number, and the system's current date and time.

**Punch and Roll Recording**

- **Pre-roll**: The amount of audio to play before recording. Default is 5
  seconds.

- **Crossfade**: The length of crossfading Tenacity will apply between the
  prevoius clip and the recorded clip.

## MIDI Devices

``` {note}
This preference panel is only available in nightly builds if MIDI is enabled.
```

**Interface**

- **Host**: The MIDI host to use.

**Playback**

- **Device**: The MIDI playback device to use.

- **MIDI Synth Latency**: The latency for the MIDI playback device in
  milliseconds (ms). Default is 5 ms.

## Quality

**Sampling**

- **Default Sample Rate**: The default sample rate for new projects. Default is
  48000 Hz.

- **Default Sample Format**: default sample format used for audio. Can be
  16-bit, 24-bit, or 32-bit float.


**Real-time Conversion**

```{note}
These settings are used for realtime actions such as playback.
```

- **Sample Rate Converter**: The quality of the sample rate converter used.
  Default is Medium Quality.

- **Dither**: The type of dithering, if any, used when converting sample rates.
  Default is none.

**High-quality Conversion**

```{note}
These settings are used for non-realtime actions such as exporting.
```
- **Sample Rate Converter**: The quality of the sample rate converter used.
  Default is Best Quality.

- **Dither**: The type of dithering, if any, used when converting sample rates.
  Default is none.

## Interface

**Display**

- **Language**: The language the UI should use.

- **Location of Manual**: The version of the manual to use. This can be the
  manual hosted on our site (which is always for the latest stable version of
  Tenacity) or a local copy if installed.

- **Theme**: The theme Tenacity should use. To use your own theme, create a new
  folder called 'Theme' in your configuration directory and copy the
  ImageCache.png file to it. Then, select 'Custom' to use the new theme.

- **Meter dB range**: the maximum range of the input and output meters in the
  meter toolbar in decibels.

**Options**

- **Show 'How to Get Help' at launch**: Shows the 'Welcome to Tenacity!' dialog
  whenever Tenacity starts up. In pre-releases, such as in alpha or beta
  versions, it also shows the 'Tenacity Pre-Release' dialog.

- **Show extra menus**: Shows the 'Extra' menu. This menu contains additional
  options that you may not find in any other menus by default.

- **Show alternative styling (Mac vs PC)**: Inverts the styling of all toolbar
  buttons and other theme resources that have Mac-specific counterparts. Most
  themes except the Audacity and Audacity Classic themes have only slight
  differences when enabled, but all themes have smaller buttons in the
  Transport Toolbar. The Audacity and Audacity Classic themes use Aqua-themed
  buttons, in the transport toolbars. This has different effects depending on
  your platform:

  - **On Mac**: Tenacity uses regular theme resources when enabled.

  - **On non-Mac**: Tenacity uses Mac-specific theme resources when enabled.

- **Beep on completion of longer activities**: If enabled, Tenacity will play a
  beep on activities that take longer than a minute.

- **Retain labels if selection snaps to a label**: If enabled, labels will be
  kept when deleting audio in the same selection. Disabled by default.

- **Blend system and Tenacity theme**: Blends Tenacity's selected theme with
  the system theme.

- **Use mostly Left-to-Right layouts in RTL languages**: Forces left-to-right
  layouts in RTL languages.

**Timeline**

- **Show Timeline Tooltips**: Enables tooltips in the Timeline.

- **Show Scrub Ruler**: Shows the Scrub Ruler. The Scrub Ruler allows you to
  scrub back and forth between audio at variables speeds.

## Tracks

**Display**

- **Auto-fit track height**: Automatically resizes all tracks to fit the entire
  height of the track area. Disabled by default.

- **Show track name as overlay**: Overlays the track's name in the track.
  Disabled by default.

- **Use half-wave display when collapsed**: If enabled, only the upper half of
  an audio clip is displayed when collapsed. Disabled by default.

- **Auto-scroll head if unpinned**: Auto-scrolls the track area during playback
  if the playhead is unpinned. Enabled by default.

- **Default view mode**: Selects what mode to display new audio tracks as by
  default. The default value is a waveform, but you can change it to a
  spectrogram or multiview (both a waveform and spectrogram at once).

- **Default waveform scale**: The default scale the vertical ruler should use
  for new audio tracks. This is a linear scale by default, but this can be
  changed to a logarithmic scale (i.e., decibels) in case that's more
  desirable. (Note that using decibels distorts the RMS).

- **Display samples**: How individual samples, when zoomed in that far, should
  be displayed. The default is a stem plot, which draws samples with a vertical
  line "stemming" from the point to the vertical center of the track. If you
  switch this to "Connect dots", this draws the samples as dots connected to
  each other.

- **Default audio track name**: The default name of new audio tracks.

**Zoom Toggle**

This behavior controls the _Zoom Toggle_ button in the edit toolbar, which
toggles between two difference modes. The first time it's clicked, zoom preset
1 is engaged, while zoom preset 2 is engaged when clicked again, and so on.

- **Preset 1**: The behavior when the _Zoom Toggle_ button in the edit toolbar
  is clicked. This is set to "Zoom Default", which resets the track area's zoom
  to its default level.

- **Preset 2**: The behavior when the _Zoom Toggle_ button in the edit toolbar
  is clicked after the first time. This is set to "4 Pixels per Sample" by
  default, which sets the track area zoom level to 4 pixels per sample for
  every audio track, enough to where you can manipulate individual audio
  samples.

### Tracks Behaviors

- **Select all audio, if selection required**: On actions that require a
  selection, selects the entire project if no audio is selected. This is off
  by default.

- **Enable cut lines**: When audio is cut, if enabled, Tenacity will put a
  a cut line where the audio was cut. If this line is clicked, the cut audio
  will be expanded. This is disabled by default.

- **Editing a cilp can move other clips**: When pasting a clip in a spot
  without sufficient room, Tenacity will automatically move other clips if
  enabled. Similarly, if a cut line does not have enough room to expand, any
  surrounding clips will automatically be moved as needed. This is disabled by
  default.

- **"Move track focus" cycles repeatedly through tracks**:

- **Type to create a label**: If enabled, typing will automatically create a
  new label. Disabled by default.

- **Use dialog for the name of a new label**: If enabled, Tenacity will use a
  dialog for renaming audio clips and adding new labels. Disabled by default.

- **Enable scrolling left of zero**: Allows scrolling into negative time. Note
  that you can still move clips before 0. Disabled by default.

- **Advanced vertical zooming**: Enables advanced zooming features in the
  vertical ruler for audio tracks. Features include click to zoom and drag to
  zoom along with right-click to fit. Disabled by default.

- **Solo button**: Changes the behavior of the solo button. If set to "simple",
  only one track can be soloed at a time. If set to "Multi-track", multiple
  tracks can be soloed at the same time. You can even disable it entirely if
  you want.

### Spectrograms

This section contains different spectrogram settings. You may also notice a new
"Preview" button in the bottom left corner of the Preferences window. Clicking
this button allows you to preview your new spectrogram settings without leaving
Preferences.

#### Scale

- **Scale**: The scale the vertical ruler should use for spectrograms. Default
  is "Linear".

- **Min Frequency (Hz)**: The minimum frequency to display in spectrograms for
  new tracks. Default is 0.

- **Max Frequency (Hz)**: The maximum frequency to display in spectrograms for
  new tracks. Default is 20000.

```{note}
The maximum spectrogram frequency was only 8,000 Hz prior to 1.3.4.
```

#### Colors

- **Gain (dB)**: Increases or decreases the brightness of the spectrogram by
  adding additional gain in computing the spectrogram. The default value is 20
  dB. Values cannot be negative. By increasing this value, the spectrogram
  brightness is increased. By decreasing this value, the spectrogram brightness
  is decreased.

- **Range (dB)**: Increases or decreases the range of signals displayed in the
  spectrogram. The default value is 80 dB. Value must be at least 1 dB. By
  increasing this value, more signals are colored in the spectrogram, and vice
  versa.

- **High boost (dB/dec)**: Adds additional gain to higher frequencies (i.e.,
  frequencies above 1,000 Hz). The default is no additional gain. Values cannot
  be negative.

- **Scheme**: The color scheme to use for spectrograms. Default is color. You
  can also choose from the classic color scheme (theme defined; see below) to
  grayscale options (regular or inverse).

```{warning}
* The first three options are unavailable if the spectrogram algorithm chosen
is "Pitch (EAC)".

* The "classic" color scheme relies on the current theme to define a spectrogram
color scheme. Its accuracy is fairly limited and thus deprecated. It is slated
for removal in the 1.4 release.
```

#### Algorithm

This section contains various settings for computing the spectrograms.

To compute a spectrogram, audio data is first divided into several chunks or
"windows". The size of each chunk is known as the **window size** while the way
it is computed is known as the **window type**.

- **Algorithm**: This can be one of three settings - Frequences (default),
  Reassignment, and Pitch (EAC).

``` {note}
Choosing 'Pitch (EAC)' will disable most color settings except for color
scheme.
```

- **Window size**: The chunk size to use in computing the spectrogram
  (specifically for the FFT). Higher values mean better vertical detail, or
  higher _resolution_, while lower values mean lower resolution.

- **Window type**: The method to compute the spectrogram. The default is
  "Hann".

#### Miscellaneous

- **Enable Spectral Selection**: Allows selecting a frequency range on a
  spectrogram, including an upper, lower, and center frequency. This selection
  is shown in the spectral selection toolbar, wihch is hidden by default.

## Import/Export

- **When exporting tracks to an audio file**: Allows you to select either
  between mixing down to stereo or mono audio or using the Advanced Mixing
  Options dialog.

- **Show Metadata Tags editor before export**: Enables the Metadata Tags editor
  dialog when exporting audio. Enabled by default.

- **Ignore blank space at the beginning**: If enabled, Tenacity skips all
  empty space at the beginning of the project when exporting it. Disabled by
  default.

- **Exported Label Style**: Allows you to select what label style label for
  export. The default style exports only the label name and its time range
  while the extended style adds any frequency selection to the exported labels.

- **Exported Allegro (.gro) files save time as**: Selects which time format
  Allegro files should store time as. Default is seconds.

## Extended Import

This preference subpane allows you to add rules for import filters. You can
specify custom import filter rules here.

## Libraries

Only FFmpeg settings are found here. In 1.3 beta 1, they used to contain
settings for loading an external LAME library. However, starting in 1.3 beta 2,
they were removed as support for LAME loading was removed at the same time.

There are two buttons:

- **Locate...**: Allows you to set the FFmpeg library location. If valid
  libraries are found, you will be prompted if you want to continue anyway.

- **Download**: Opens the 'FFmpeg' Tenacity manual page, which contains a link
  to download our FFmpeg releases.

For more information about using FFmpeg with Tenacity, please see
[FFmpeg In Tenacity](#ffmpeg).

## Directories

- **Default directories** - Allows you to specify default directories for
  opening and saving projects, importing and exporting files, and macro ouput.
  If left blank (default), the last folder selected for these operations is
  used.

**Temporary files directory**

- **Location**: The location of Tenacity's temporary files directory. This
  location should be persistent across system reboots as this is used for
  recovering unsaved projects. (Directories that may be cleared across reboots,
  such as ``/tmp``, are not suitable for this purpose).

```{note}
Tenacity also shows the amount of free space available in the selected
folder.
```

## Warnings

This page contains several options to show warnings whenever performing am
action.

- **Saving projects**: Warns when saving a project as not a regular audio file.

- **Saving empty project**: Warns when unsaved empty projects.

- **Mixing down to mono during export**: Warns when mixing down stereo audio to
  mono audio.

- **Mixing down to stereo during export**: Warns when mixing down multi-channel
  audio to stereo audio.

- **Mixing down on export (Custom FFmpeg or external program)**: Warns when
  mixing down audio when exporting via an external program.

- **Missing file name extension during export**: Warns if there is no file
  extension when exporting audio.

## Effects

This page contains options related to enabling various plugin formats and
grouping effects in the *Effects* menu. You can enable the following effects:

- LADSPA
- LV2
- Nyquist
- VST
- Vamp

```{note}
Tenacity does not support VST3 effects.
```

**Effect Options**

- **Sort or Group**: How effects should be sorted or grouped.

- **Maxium effects per group**: The maximum number of effects to group
  together. Setting this to '0' disables grouping.

## Theme

This page exposes some advanced options for Tenacity's theming system. They are
intended for advanced users, theme designers, and contributors.

Settings here have nothing to do with theme selection. See the Interface section
instead.

**Theme Cache - Images & Color**

```{note}
All files are saved or loaded in the 'Theme' folder in your configuration
directory unless otherwise specififed.
```

- **Save Theme Cache**: Saves the current theme to an ImageCache.png.

- **Load Theme Cache**: Loads an ImageCache.png if present.

- **Output Sourcery**: Outputs a ThemeAsCeeCode.h header. The resulting file can
  be included with Tenacity's source code.

- **Defaults**: Resets ALL theme preferences.

**Individual Theme Files**

- **Save Files**: Saves all individual theme bitmaps (e.g., the play button).

- **Load Files**: Loads individual theme bitmaps into Tenacity.

## Keyboard

This page allows you to view and set key bindings for various actions in
Tenacity. You can also import and export key bindings in Tenacity as well and
set defaults.

## Mouse

This preference page shows all mouse bindings in Tenacity and is similar to the
Keyboard preferences. However, mouse bindings cannot be changed.

## Modules

```{warning}
These settings are for an **experimental feature**. They are subject to
change at any time and might be undocumented. Only use this if you know what
you are doing.
```

This preference page allows you to enable or disable external modules. Modules
are an experimental feature in Tenacity and are not guaranteed to be stable.
For module developers, there is currently no stable interface.

**Module options**

Aside from the standard *Enabled* and *Disabled* options, which enable and
disable the module respectively, you have the following options:

- **Ask**: Tenacity will ask on start up if you want to load a module.

- **Failed**: Tenacity believes the module is broken and cannot be loaded.

- **New**: No choice has been made yet.

Enabling or disabling a module requires a restart to take effect.

**Aavailable Modules**

Currently, there is only one module that is shipped with Tenacity:
mod-script-pipe. This allows external scripts or programs to run various
commands within Tenacity.

```{warning}
**`mod-script-pipe` DOES NOT SANITIZE ITS INPUTS, WHICH CAN BE DANGEROUS!** For
this reason, DO NOT use Tenacity with mod-script-pipe in a production
environment (e.g., on a remote server). Using Tenacity with mod-script-pipe
for your own personal use is fine, however.
```

## Errors You May Encounter

### Inaccessible or Unwritable Preferences

Usually, you should not encounter this error. If you do encounter this error,
it can mean a few things:

* You deleted the Tenacity settings file.
* Some software is interfereing with Tenacity's preference file or the folder
  where it resides.
* The folder where Tenacity's preferences resides became unwritable.

If you deleted Tenacity's preference file or the folder where it resides, you
may get an error on exit. This will happen if this occurs. If you did _not_ do
this, however, then it can be an indicator of a larger issue. For example, you
could have some virus, malware, or other unwanted software installed on your
computer that could be interfering (intentional or not) with Tenacity's
preferences.
