# Scripting

Tenacity contains an external interface that allows external scripts and
programs to execute commands. This interface is provided by mod-script-pipe.

mod-script-pipe comes from Audacity, and being a fork, most scripts designed
for Audacity should work with no issue in Tenacity.

`mod-script-pipe`, as the name implies, is an external module; in order to use
it, you must enable and load it first. To enable `mod-script-pipe`, go to
*Preferences > Modules* and change `mod-script-pipe` to "Enabled". Set it to
"Disabled" to disable external scripting.

The main purpose of mod-script-pipe was for use by Audacity developers, and
thus it's not intended for average end-users. At some point, we intend to
stabilize the module interface, but that may take a while before we have a
truly stable module interface.

```{warning}
Enabling mod-script-pipe allows other applications **full control** over
Tenacity, which can be used for malicious purposes. When a program connects
to `mod-script-pipe`, you will NOT be notified, and neither can you restrict
an application's abilities.
```

```{danger}
**DO NOT USE** ``mod-script-pipe`` **ON A WEB SERVER!** It was NOT designed for such
usage (e.g., it does **not** sanitize inputs) and probably will never be.
```

## Pipe Names

Tenacity has two pipes, one to send data to, and one to receive data from. The
names vary depending on your platform. See the relevant sections below to learn
what name the pipes are for your platform.

### Windows

**To**: `\\.\pipe\ToSrvPipe`
**From**: `\\.\pipe\FromSrvPipe`

### macOS, Linux, and Other OSes

**To**: `/tmp/tenacity_script_pipe.to.[uid]`
**From**: `/tmp/tenacity_script_pipe.from.[uid]`

Replace `[uid]` with the user's ID. A full pipe name could look like this:
`/tmp/tenacity_script_pipe.to.1000` and `/tmp/tenacity_script_pipe.from.1000`.

## Command Syntax

When sending a command over the pipe, the command syntax is as follows:

`CommandName: [Parameter=Argument]`.
