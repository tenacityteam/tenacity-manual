# Importing and Exporting

This chapter covers importing and exporting in Tenacity. There are several
options to cover in both importing and exporting throughout Tenacity.

The most common importing and exporting workflow follows this: import audio
(say an MP3), do your edits, export (maybe back to an MP3 also) and you are
done. Unless you are importing raw data or certain Matroska files, there are
no importing options you can choose from. Exporting, however, has numerous
options depending on what you are trying to import.

You can also import data other than audio. For example, you can import MIDI
data or label data (exported from Tenacity or Audacity) as well.

## Terminology

Before we get started, you should be familiar with some terminology. When we
refer to a codec or format as being _natively_ supported by Tenacity, we mean
that the format or codec is natively supported by Tenacity without FFmpeg. To
_natively import_ a file means to use one of Tenacity's built-in importers to
import a file. To _natively export_ a file means to use one of Tenacity's
built-in exporters to export a file.

## Importing

Tenacity's import options can be found under _File > Import_, where there are
the following options:

* Audio
* Labels
* MIDI
* Raw Data

For audio and MIDI (if enabled), you can also use drag-and-drop by dragging the
audio or MIDI file you want to import over the track area. In fact, this might
have been the first time you imported audio in Tenacity.

### Audio

Mostly everything is straightforward, but you should be aware of a few notes
when using older versions (i.e., versions before 1.3.4) of Tenacity:

* On Windows, Tenacity's Matroska importer may cause the entire program to
  crash if there is a chance the file to import may be mistakened as a Matroska
  file. The bug could also be present on other platforms, but this is fixed in
  1.3.4.

* The Arch Linux package (**NOT** AUR package) was built upstream libid3tag.
  Arch later switched their libid3tag to our fork, which contains an applied
  ABI-breaking patch. 1.3.4 is unaffected because it was built against our
  fork. Architectures other than x86_64 _may_ also be unaffected.

In the future, we plan to expand Tenacity's 

#### Matroska (MKA/MKV)

The one exception is Matroska. If you have either a PCM-encoded or FLAC-encoded
Matroska file, you're in luck! Tenacity _natively_ imports those files rather
than passes them to FFmpeg. Otherwise, they get passed to FFmpeg.

If you are natively importing a Matroska file, you may notice an additional
popup asking which streams(s) to import. Audio tracks will start with "Index"
while label tracks will start with "Label". It is recommended to select the
last track with "Label" to import all tracks.

### Importing Raw Data

Tenacity has a special feature of importing raw data. The _Import Raw Data_
dialog has multiple options that affect how Tenacity interprets the data.

Through this way, you can also perform something called _data bending_, which
allows you to modify, potentially corrupting, a file not normally supported by
Tenacity. You can achieve pretty cool visual affects with images, for example.

``` {warning}
We cannot guarantee the outcome of databending. While we encourage you to
experiment with various editing features and effects to create something
unique and awesome, it is also potential that certain edits do nothing.

We are looking for community-contributed guides from the manual, including
those on databending. Have a cool guide? Visit
[our repo](https://codeberg.org/tenacityteam/tenacity-manual) to contribute
your awesome guide!
```

```{note}
TODO: document raw data options.
```

### Errors While Importing Raw Data

Normally, you shouldn't need to use this feature if you are importing regular
audio unless you know what you are doing. However, it is possible to encounter
an error that may say, "Bad data size. Could not import audio".

According to the Audacity 3.1 manual, this error could only be reproduced via a
"doctored bogus non-WAV file". It is more than likely you will never encounter
this error, especially with normal usage. However, if you encounter this error,
please let us know and what you did to encounter this error.

## Exporting

Each exporter has its own set of options.

```{note}
TODO: Add generic and exporter-specific options
```

### Matroska Options

Tenacity's Matroska exporter can only export PCM and FLAC audio. However, it
also has a nice feature none of our other exporters have. It allows you to
export any label tracks as chapters. Simply check "Keep Labels" to export all
labels as chapters.

## Importing and Exporting with FFmpeg

If you are importing an obscure, proprietary, or otherwise non-native format,
FFmpeg should have you covered. Please see [FFmpeg](#ffmpeg) for more details
on using FFmpeg. Using it is almost transparent.
