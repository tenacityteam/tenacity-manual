# Quick Help

If you need additional help with Tenacity that this manual is not able to
provide, you have several other methods of getting help:

- Our Matrix chatroom.
- Our IRC chatroom (#tenacity on Libera.Chat; not as active)
- Unofficial communities

Our Matrix chatroom is the best place to get help. We will try to address your
question as best as we can, but please be patient in that your question might
not be addressed right way. Alternatively, IRC is also an option, but ever
since [the Libera.Chat Matrix bridge was shut down](https://libera.chat/news/matrix-bridge-disabled-retrospective),
we have not been as active on IRC compared to Matrix. If you want to join us on
IRC anyways, join #tenacity and someone might be able to support you.

Finally, unofficial communities might provide you with some support. Keep in
mind that these communities are, as their name suggests, *unofficial*. We are
not responsible for their moderation nor do we control their content, and we
might not always respond to support requests. Nevertheless, they are another
way you can receive support, and some of us may respond to them. A few
unofficial communities include the following:

- [r/TenacityAudio on reddit](https://redit.com/r/TenacityAudio)
