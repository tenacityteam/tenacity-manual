# Editing Part 2: Concepts in Action

Now that you know the basic conepts, it's time to see them in action.

To get started, first import, record, or generate some audio. Then read the
sections below.

## The Edit Cursor

The edit cursor is the long vertical line that is overlaid across all tracks
in the track area. Simply click anywhere on any track to move the edit cursor.

If you prefer to move the edit cursor via keyboard, press the _Left Arrow_
button on your keyboard to move the edit cursor left. Press the _Right Arrow_
button on your keyboard to move the edit cursor right.

## Selecting Audio

While this is one of the most basic features of Tenacity, it is also one of the
most flexible. Simply hover your mouse over a clip (away from its handle),
click on it, and drag it over the desired audio selection.

If you want to extend your selection, hover your mouse over either edge of the
selection and repeat the other two steps above. Alternatively, you can hold
shift and press either the _Left Arrow_ key to move the left edge of the
selection or the Right Arrow key to move the right edge of the selection,
respectively. If you want to reduce your selection, hold _Ctrl_ (_Cmd_ on Mac)
along with _Shift_ when pressing the _Left Arrow_ key to reduce the _right_
edge of the selection or the _Right Arrow_ key to reduce the _left_ edge of the
selection. Essentially, when you add _Ctrl_, it reduces your selection and
inverts what edge the _Left Arrow_ and _Right Arrow_ keys work on.

To delete a selection of audio, with your selection active, press the _Delete_
key. This delete anything in the selection, including free space.

When you move a partially-selected clip by the portion of its handle in the
selection, the selection and its contents will be moved.

## Editing Individual Samples

Tenacity allows you to edit individual samples via the Draw Tool. Before you
can use the tool, however, you need to zoom in enough to see the individual
samples.

To use the tool, after following the preparation above, switch to the Draw Tool
by clicking the pencil icon in the edit toolbar (next to the record button by
by default) or by pressing _F3_. Then, click and drag your mouse cursor over
the points you want to modify.

## Duplicating Audio

An easy way to duplicate audio is to copy either an entire clip or a
selection and paste it to a new track. However, Tenacity allows you to more
easily do this through duplicating a selection.

To start, first selection part of a track you want to duplicate, or click the
_Select_ button on a track to select the entire track. Then, either go to
_Edit > Duplicate_ in the menubar or press _Ctrl + D_ on your keyboard. This
duplicates your selection onto a new track along with adding the duplicated
contents to your current selection. Note that the new track the duplicated
contents will reside on will be named after the original track where the
original contents reside. You can also rename a track by clicking on the track
options dropdown (the track's name) and clicking _Name..._.

## Cutting, Copying, and Pasting

Tenacity allows you to cut, copy, and paste data from several tracks to other
tracks. You can copy audio clips, MIDI clips, or labels.

Standard copying and pasting shortcuts (i.e., _Ctrl + X_, _Ctrl + C_, or
_Ctrl + V_ and their Mac equivalents) are supported. You can also use the
_Edit_ menu or the Edit Toolbar for copying and pasting too.

Of course, you must have something selected in order to copy or cut it. See
[Selecting Audio](#selecting-audio) for more details.

``` {note}
Clip types must match. Pasting a MIDI clip into an audio track, or vice versa,
is not allowed. Similarly, attempting to paste a label into anything other than
a label track is also not allowed.

If you were brought here by an error message help button, you were attempting
to paste a clip into a different track type.
```

### Pasting Stereo Clips into Mono Tracks

Pasting a stereo clip into a mono track is disallowed because it would be
ambiguous; what channel from the stereo clip should be pasted into the mono
track? Should it be the left channel or the right channel? Should both channels
be mixed down to mono instead? (The latter isn't easy to implement). We can
define a default behavior, but some users could get confused by the default
behavior.

Indeed, there are enhancements (e.g., asking what channel should be pasted)
that can be made. If you have an idea, always let us know what you think.

## Common Errors You May Experience

Chances are, if you were brought to this section, you likely came from one of
Tenacity's help buttons.

### Insufficient Track Space

This error can occur when attempting to paste a clip in a region that isn't
large enough. By default, Tenacity doesn't move clips to accomodate others in
order to fit in a certain spot in the timeline.

If you want Tenacity to automatically move clips to accomodate others in order
to fit in a certain spot on the timeline, go to _Preferences > Tracks > Tracks
Behaviors_ and check _Editing a clip can move other clips_. See the
[Preferences](#Preferences) page for more details.

### A File Failed to Open or be Read From

If Tenacity fails to _open_ a file, it may be that another application is using
it, at least on Windows. Other possible causes include the file being
non-existent.

If Tenacity fails to _read_ a file, it may be caused by something else.
Usually, this is caused by a corrupted project file. Worst case, it could
indicate a drive failure, although that's much less likely to happen than a
corrupted project.

### Your Disk is Full or Not Writable
If Tenacity has trouble writing to a file on a drive, it could mean either of
the two causes in the error message:

* You don't have enough free space on the drive to save the project
* You don't have write permissions to the location you are trying to save the
  project to.

#### Lack of Free Space
AUP3 projects can be large in size even for modest works. Ensure that you have
plenty of free space on the drive if you plan to work with your project on that
drive.

If you are saving to an external drive, try moving some files off it to free up
space. You can move them back later. If the drive has no other files on it, you
need a larger drive.

If the drive has enough space for you to save your project, it may be that the
location is read-only. Make sure you have the appropriate permissions to write
to the drive. Check the location's permissions to ensure you have sufficient
write access to save your project.

If you continue to have problems and you've already checked the above problems,
please feel free to ask for help.


### FAT32 Drives
Tenacity disallows you from saving on a drive formatted as FAT32. Project sizes
can grow pretty quick, especially with AUP3 projects. It also disallows
changing your temporary files directory to that on a FAT32 drive.

FAT32 drives are unsuitable for saving projects and temporary files because of
a 4 GB file size limitation. your use case may never cause Tenacity to generate
such large files, it is better safe than sorry for other users where their use
case causes Tenacity to generate files that large.

``` {warning}
On Haiku, Tenacity will **NOT** warn you if you are saving to a FAT32 drive.
Similarly, Tenacity would not warn you either on FreeBSD until 1.3.4.

Just because it works on other platforms does _not_ mean it's recommended you
do this. In fact, the next version of Tenacity will start prohibiting FAT32
drives on Haiku.
```

### Error Trying to Open a Project

Usually, Tenacity should be able to open AUP3 projects from Audacity; other
forks, including Audacium and Saucedacity; and older versions of Tenacity
(e.g., from legacy Tenacity builds or 1.3+ builds). If opening projects from
newer versions of Audacity, you may lose features if you save the AUP3 project
from Tenacity and go to reopen it in Audacity.

#### Database Errors

If you get a database error message, you've encountered a rare situation.
Please join us on our Matrix channel to report this error or send us a message
over Mastodon/the Fediverse. Alternatively, you can create a bug report if you
think this is a bug.

#### Cannot Import AUP3 Format

You cannot import an AUP3 project, but if you get this error, it is likely you
tried to use drag-n-drop for importing the project file. This is currently not
supported.

#### Cannot Import AUP Project

If you can't import an AUP file, it may be corrupted or unreadable by Tenacity.
If you can open the project in the version of Audacity last used to save the
project, it is possible that Tenacity might not support the project. You might
want to save the project with a later version of Audacity (i.e., version 2.4.2)
before importing it into Tenacity.

#### Reference to invalid character number at Line X

This likely means the project is corrupted.

#### Not Well-Formed (invalid token) at Line X

This can indicate potential corruption.

## Other Errors

### Resampling Fails

Usually, resampling should succeed, but it is possible to encounter this rare
error.

If a resample error happens, either Tenacity's resampler produced a negative
number of samples or it attempted to resample samples it did not have.
