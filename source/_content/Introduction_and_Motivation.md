# Introduction

Welcome to Tenacity, a free and open-source audio editor for Windows, macOS,
Linux, *BSD, and Haiku! With it, you can perform many tasks, from recording
your old CD collection to mastering a podcast episode using a myriad of
different effects to using it as a research aid when analyzing or crafting
specific audio.

Tenacity is more than just an audio editor, although we're well known for it.
We also maintain forks of several libraries, including the well-used libmad and
libid3tag libraries. You need not know what these libraries are or what they
do, except that Tenacity uses them itself unsurprisingly. The reason why we
mention them, however, is because several packagers, including those for well-
known Linux distributions such as Fedora and, more recently, Debian, package
our forks of these libraries. There are plenty of others that package them too,
but since there are too many to list here, we will redirect you to
[this page](https://repology.org/project/libid3tag/) that details libid3tag's
packaging status and [this page](https://repology.org/project/libmad/) that
details libmad's packaging status. Beyond these libraries, some of us have also
contributed patches to upstream projects we depend on, such as PortAudio. We
prefer to use upstream sources wherever possible, so if there's something that
needs to be done upstream, we may submit a PR to help out.

Tenacity is based on Audacity, and there's a very specific reason why the
project was started. The full details are in the section below, but if you are
short on time, the primary reasons were attempts at adding telemetry and a new
desktop privacy policy. If you are seeking the full details, we encourage you
to read the section below and seek out additional sources so you have an
accurate view of what happened. Do not rely on this manual as your only source.

In most ways, Tenacity can serve as a drop-in replacement with Audacity.
However, you must know that Tenacity development was on hiatus for a period of
time, and during that time Audacity made several developments that Tenacity
does not (yet) have. As new versions of Tenacity are released, we aim to
mostly preserve compatibility with newer Audacity versions.

The end goal of this manual is to teach users the "ins and outs" of Tenacity;
in other words, we aim to take novice users and transform them into
intermediate or advanced users. Of course, if you want to make yourself
knowledgeable around Tenacity, it will certainly take some time. Thus, you
don't have to read the entire manual if either you don't have time, you don't
want to, or for any other reason. Nevertheless, we hope that the manual
provides sufficient explanation about using Tenacity, what it can do, and how
to use it effectively.

Finally, remember that the manual is always a work in progress. For this
version and edition, this is no doubt the case as you may see several sparsely-
documented pages or even sections with just "TODO" in them. Writing
documentation is no easy task. Writing good documentation takes even more time.
If you are interested in contributing an improvement to the manual, we welcome
your input through issues over at the manual's issue tracker,
https://codeberg.org/tenacityteam/tenacity-manual/issues.

## Motivation and History of Tenacity

Perhaps a very appropriate start to Tenacity is its background and history.
Recall that there was a reason why the project was started in the first place.

This section covers some history of Tenacity, including its origins, start, and
various merges, past and present, that still continue with it at this current
time in writing.

### Definition of "tenacious"

Our name says a lot if we take a look at the definition of "tenacious":

> 1. a: not easily pulled apart
>       *a tenacious metal*
> 
>    b: tending to adhere or cling especially to another substance
>       *tenacious burs*
> 
> 2. a: persistent in maintaining, adhering to, or seeking something valued or desired
>       *a tenacious advocate of civil rights*
> 
>       *tenacious negotiators*
> 
>    b: retentive
>       *a tenacious memory*


_“Tenacious.” Merriam-Webster.com Dictionary, Merriam-Webster,
https://www.merriam-webster.com/dictionary/tenacious. Accessed 4 Dec. 2022._

Keep this definition in mind as you read the background of Tenacity.

### History

In April 2021, Muse Group acquired the famous audio editing application
Audacity. Their goals for Audacity were to bring much needed improvements to
Audacity. However, not too long after, there was an attempt to add telemetry to
the program, spurring controversy among its community. Luckily, the new
developers backed off on their proposal, only maintaining basic error reporting
and update checking where no personal information is collected.

However, in May of 2021, Audacity was embroiled in controversy once again when
Muse Group imposed a new contributor license agreement (CLA) allowing them to
reuse Audacity code in proprietary software, bypassing the copyleft protections
of Audacity's GPL license. Shortly thereafter in July 2021, Muse Group
introduced a new privacy policy, recommending that users under the age of 13
not use the program. This raised concerns of violating Audacity’s GPL v2
license. While the new privacy policy was updated to address these concerns,
the community already had enough.

After all of the controversies, Tenacity was born. It first started as
_temporary-audacity_ on GitHub since it didn't have a name. In order to
decide a new name for the project, the lead maintainer at the time held a vote.
Among the new names were "Audacium", "Sneedacity", and "Tenacity". The name
Sneedacity would later gain traction among 4chan members, resulting in a large
volume of votes for the name Sneedacity.

In response to the large volume of votes by 4chan members, the previous
maintainers had an emergency vote, choosing the name Tenacity instead of
Sneedacity. This upset some, leading to the creation of a new fork with
virtually the same intentions. Unsurprisingly, this fork was named Sneedacity.

Initially, there was spamming and trolling in the Tenacity repo. As time
passed, this activity died considerably, but so did activity on all other
fronts too. There were other problems also, such as the fragmentation of
Audacity forks with the same goals and intentions, where not one was a clear
alternative to Audacity for those who lost trust in the new developers. There
were four primary forks that had seen at least some degree of activity:
Audacium, Tenacity, Sneedacity, and Saucedacity although other forks, such as
AudMonkey, that received some activity but seemingly ceased all activity for
(un)known reasons. Initially, Tenacity received quite a bit of activity,
receiving all kinds of contributions from all kinds of different people.
Tenacity received a new build system that devendored almost all dependencies,
which was made by Be (a Mixxx developer); a new dynamic compressor, originally
intended for Audacity, written by Max Maisel (an Audacity contributor), and
several other valuable and important contributions. Anyone that contributed put
their heart and soul into this, usually without anything in return.
Nevertheless, despite the blood, sweat, and tears put into Tenacity by these
wonderful contributors, Tenacity development was later put on hiatus due to the
lack of activity.

Later, in July 2022, user FrostKnight on GitHub noticed that Saucedacity was
still actively maintained compared to all other forks at the time. This
inspired the maintainer to take another look at Tenacity. At the same time,
some Tenacity contributors started to look at Saucedacity and get involved. A
couple of weeks later, the maintainer of Saucedacity contacted the Tenacity
maintainers at the time to merge the two projects together. Months later, the
merge was finally completed on November 26, 2022, with the Saucedacity
maintainer going on to become the new Tenacity maintainer, also making Tenacity
contributors new maintainers as well.

Saucedacity wasn't the only project Tenacity merged with, however. Audacium also
merged with Tenacity, with their maintainer becoming a Tenacity maintainer.
Audacium's themes were merged into Tenacity in addition to carrying over
Audacium's default sample rate.

Indeed, our project history correlates to our name; we are persistent,
retaining, and cannot be pulled apart.
