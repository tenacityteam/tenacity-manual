# ASIO in Tenacity

Prior to 1.3.4, the only way you could get ASIO support in Tenacity was through
compiling an ASIO-enabled PortAudio DLL yourself and replacing the default one
shipped with Tenacity. Starting in 1.3.4, you can now build Tenacity without
having to build an ASIO-enabled PortAudio DLL first.

Keep in mind that this page is mainly intended for advanced users. If you know
nothing about compiling software, you should go learn how to compile Tenacity
from source first. Then come back to this page once you are confident enough.
Remember that we are here to help you.

```{warning}
The ASIO SDK license imposes restrictions on the resulting build of Tenacity.
Therefore, you cannot distribute ASIO-enabled builds.
```

## Building Tenacity with ASIO support (1.3.4+)

This method only works if you're building 1.3.4 or any later version.

Unfortunately, at this time of writing, you must use our vcpkg fork (found at
https://codeberg.org/tenacityteam/vcpkg) for this to work. This only matters if
you are using upstream vcpkg or another custom fork.

First, follow the instructions in `BUILDING.md` for cloning the Tenacity source
tree. Don't forget to clone the submodules too. If you've previously built
Tenacity before, it is recommended to start with a new build folder. Then, open
`vcpkg.json` in your favorite text editor (Notepad works fine) and find this
line:

```
"portaudio",
```

Once you've found that line, change it to the following:

```
{
   "name": "portaudio",
   "features": ["asio"]
}
```

Save your changes and then proceed to build Tenacity as usual. You can
optionally build an installer (`cmake --build C:\path\to\build\dir`) to make
builds more easily available to you.

## Replacing the Stock PortAudio DLL in an Existing Installation (<1.3.4)

Alternatively, you can also build PortAudio separately and replace an existing
installation's `portaudio.dll` with a custom version. This works for any
version, but it is highly recommend to follow the above instructions instead as
this is an unsupported method. In addition, the above method is easier as the
correct version of PortAudio is already provided for you, eliminating the need
to find the proper version.

To enable a Tenacity build with ASIO support, follow the instructions below:

1. [Build PortAudio with ASIO support](https://portaudio.com/docs/v19-doxydocs/compile_windows_asio_msvc.html).
   It is assumed that you know how to set up the build environment. If not, you
   will need to learn how to set up a proper build environment. **We will not
   accept support requests for building PortAudio.**

2. In an existing Tenacity installation, rename `portaudio.dll` to
   `portaudio.dll.bak`.

3. Copy the custom `portaudio.dll` to the build folder.

It is also recommended that you copy your custom `portaudio.dll` to a separate
folder when you update or reinstall Tenacity as the installer will overwrite
your custom version.

```{note}
At the time of writing, for step 1, it is currently recommended to build the
latest git version of PortAudio if you need to record desktop audio. Once
PortAudio 19.8 is released, it is recommended you use that version instead.
```

## Distributing Builds

```{warning}
The ASIO SDK imposes additional restrictions on distribution. We cannot legally
distribute builds with ASIO support enabled. Requests for ASIO-enabled builds
will be ignored and closed as `wontfix`.

**You cannot distribute custom ASIO-enabled builds for the same reasons
we cannot distribute such builds either.**
```
