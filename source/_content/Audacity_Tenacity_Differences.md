# Differences From Audacity


Tenacity and Audacity are very similar. Nevertheless, there are some
differences that exist between the two.

As of version 1.3, Tenacity is comparable to Audacity 3.1 feature-wise. However,
not everything found in Audacity 3.1 is implemented in Tenacity. Additionally,
features from newer versions of Audacity are not implemented in Tenacity. Some
features will not be implemented in Tenacity.

This list is likely to expand over time as contributors make changes to Tenacity
that add or remove features or modify or tweak its behavior. Additionally,
this list does not contain technical changes unless they are relevant to
user-facing behavior.

## System Requirements

- The minimum version of macOS that Tenacity will run on is now macOS 10.15.
  macOS users on 10.12 to 10.14 may use Saucedacity 1.2.1, but Saucedacity is
  unsupported. Those on versions prior to 10.12 may use Audacity 3.0.2, which
  does not contain any networking at all.

- Tenacity is officially distributed as a Flatpak. You may encounter issues with
  the sandbox, and if so, please file a bug report.

## Editing and Playback

- The edit cursor is now a new snapping guide for moving and trimming clips.
  Audacity lacks this feature.

- Audacity's new looping features (introduced in 3.1) are not present. We do not
  have plans to implement them. You may still loop audio by selecting a region
  to loop and pressing the loop button.

- Due to under-the-hood changes, the following features were either removed or
  are no longer possible:

  - The Mixer Toolbar was removed. Currently there is no way to adjust input or
    output volume levels in Tenacity.

  - On Windows, recording computer audio is no longer possible. However,
    advanced users can build Tenacity against a nightly version of PortAudio
    that restores this functionality.

- Zoom shortcuts have changed: Ctrl+= zooms in, Ctrl+- zooms out, and Ctrl+0
  zooms normal (resets the zoom). In Audacity, these are Ctrl+1, Ctrl+2, and
  Ctrl+3, respectively.

- Sync-lock can now be overriden. Hold 'Alt' while dragging a clip to override
  it.

## Analysis

- Horizontal zoom is now supported in the Plot Spectrum window.

## UI

- Added new themes.

- Added Edit Mode, which allows you to hide toolbar grabbers if disabled. Edit
  Mode is enabled by default.

## Preferences

- The buffer length can now be set in samples (default) or milliseconds.
  Under-the-hood work was also done that now makes this preference set the
  number of frames per buffer.

## Exporting

- Tenacity natively supports MKA/MKV (Matroska) import and export (including
  support for importing Matroska tracks as label tracks). Importing and
  exporting audio is limited to using PCM and FLAC codecs only.
