# Tips and Tricks

This page details several tips and tricks that you might find useful when using
Tenacity.

``` {note}
This page is written by people like you! If you believe something is missing
here, feel free to add something here! See
https://codeberg.org/tenacityteam/tenacity-manual for more information on
contribution.
```

## Latency Compensation

In some cases, you might need to overdub over a recording while Tenacity. In a
case where latency matters, you can adjust the amount of latency compensation to
better synchronize recordings with other tracks.

To adjust latency compensation, ensure you have the right input and output
devices selected. You might also find it easier to record to new tracks rather
than on the same track. Then, do the following:

1. Go to *Generate > Rhythm Track* to generate a rhythm track. Leave the
   settings to their defaults and click 'OK'.

2. Setup your input device so it records the track. For example, if you are
   using a mic, set it close to your computer's speakers so the mic picks up the
   speakers' output.

3. Begin record the output track. The input device should pick up the output
   from Tenacity. If it does not, ensure you have the right output device
   (and host, if needed) selected. You might need to perform additional
   troubleshooting steps if the problem persists.

4. Once you have reached the end of the track, stop recording. Now you should
   have two tracks: the original (top) and the recorded (bottom).

5. Select one peak in the original track. Then extend your selection to the
   same peak in the recorded track.

6. Note the start and end times in the selection bar. The difference between the
   two times (`start - end`) is your latency compensation.
