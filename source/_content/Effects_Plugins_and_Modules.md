# Effects, Plugins, and Modules

Tenacity has several built-in effects that you can use for your projects, and
it ships some Nyquist plugins as well. However, if you decide these are not
enough, or you have your own set of plugins you love to use, Tenacity supports
plugins and modules as well.

## Effects

An effect in Tenacity is something that applies an audio effect to a selected
portion of audio. Effects are provided by either plugins or modules.

To use an effect, select some audio first. Then, click on the _Effect_ menu and
browse for the effect you are looking for. You might need to go into a submenu.
Once you find the effect you are looking for, click on the menu entry and a
dialog will pop up displaying the various parameters of the effect. Sometimes,
an effect will not display a pop up at all because it has no parameters to set.
Examples of this include the Fade In and Fade Out effects. To use those
effects, select the duration of audio you want to fade in or out, and then use
those effects.

## Plugins

A plugin is an external effect that Tenacity loads on startup. Several plugin
formats exist, some of which are supported by Tenacity, and some of which are
not. Tenacity supports the following plugin formats:

* VST 2
* AU (macOS only)
* LV2
* LADSPA
* Vamp
* Nyquist

```{note}
Instrument plugin formats, such as VSTi are not supported.
```

## Modules

A module in Tenacity is a piece of code that Tenacity can load to provide
additional functionality. Tenacity also comes shipped with one module:
mod-script-pipe.

To enable modules, to go _Preferences > Modules_ and find the module you want
to enable. Then, click the dropdown next to it and select "Enabled". Then
restart Tenacity.

```{warning}
Modules are an experimental feature in Tenacity. For users, this means major
functionality could change at any time. For developers, this means the module
interface is not stable and can break between minor releases.
```

### mod-script-pipe

This module provides an external interface that allows scripts and other
programs to interact with Tenacity. It is only meant for advanced users and 
developers.

For more information on mod-script-pipe, what it does, and general scripting
with Tenacity, please see [Scripting](#Scripting).
