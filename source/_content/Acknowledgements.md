# Acknowledgements

We'd like to thank everyone who helped us put together this project. No one
person could have ever undertaken the effort to get this project to the state
it is than everyone who has contributed, from the drive-by contributor who
contributed a typo correction to the README to the Weblate translators to the
contributor who contributed a native importer and exporter for their multimedia
format. Your contributions, no matter their size, are invaluable.

We'd also like to thank the packagers and maintainers of the libraries we use.
Without these libraries, some features of Tenacity would simply not be
possible. We are also aiming to send contributions up your way to help out, so
we hope to see you soon!

Finally, we also want to take the time to thank previous Audacity contributors,
some of whom are still contributing to Audacity to this day. After all, they
were the ones that helped Audacity establish its reputation for decades. Of
course, the controversy over Audacity's current owners that erupted in 2021 are
another topic (you can see our Motivation page for more information), but we
welcome any Audacity contributor, past or present, to come and contribute to
Tenacity should you choose.

We also want to acknowledge the Audacity manual as being a source of
information for our manual too. It has helped us out in documenting the various
parts of Tenacity and will continue to help us out in further documentation.

## Gale Andrews

Gale Andrews was a significant Audacity contributor, and we'd like to share a
bit about them here. To be clear, they were never affiliated with our project,
but we also would like to preserve their legacy here too.

The following below is taken from the Audacity manual, which is licensed under
Creative Commons Attribution 3.0. It is unmodified and is presented as is below:

Gale joined the Audacity Team in 2007, was a graduate in Economics, and advised
us on support and documentation issues. He previously worked in survey research
for one of the UK's now disbanded Industrial Training Boards, then ran a
secondhand classical LP record business for some twenty years. Gale started
using Audacity back in 2005 for transferring vinyl records to computer, and
generally editing his collection of digital audio files.

### In memoriam

It is with a heavy heart and much sadness that I have to report the death of
Gale Andrews.

Gale was admitted to hospital on the 23rd of July 2017 after a long and
uncomfortable and often painful illness. He died in hospital some days later on
10th August 2017.

As noted above, Gale had worked on and with Audacity for twelve years and had
been a member of the Audacity Team for ten of those. He had been a diligent
worker providing much help and assistance on the Forum and through feedback@ -
solving technical issues for many users over the years (including myself when I
was struggling to get my own vinyl conversion project up and running and my USB
turntable working properly with Audacity).

Gale also contributed massively to the Manual and helped shape its current
quality. He also as part of quality assurance did a lot of bug testing and bug
reporting over the years contributing thereby to the quality of the Audacity
application that we have today.

He was an Audacity stalwart and a hard worker - he continued to work many hours
even during the latter stages of his sickness. The last messages on email that
we, on Team, have from him were written on his laptop from his hospital bed in
the early few days of his hospital stay.

He will be much-missed on Audacity - and indeed already is. Fortunately other
Team members have stepped up to carry on with the various tasks that Gale
formerly undertook.

R.I.P. Gale Andrews - died 10th August 2017

### Another memory

In December 2017 - we got a post on google+ from someone called "Stuart"

I know this is oldish news but I knew Gale in the early 90s from various
dealings I had with him as we were both LP dealers. I last saw him in 1996 and
was last in touch with him in 2003. He was probably the top classical LP dealer
of those times and lived in a terraced house in Watford - there was virtually
nowhere to sit down as his house was stacked 6 feet high with LPs. No kidding.
I estimate his LP stock must have been worth £200K. His knowledge on classical
LPs was unrivalled and I often consulted with him. Yes he did live a reclusive
lifestyle - did not drive - moved around on the train and carried bought LPs in
old carrier bags. He had the appearance, dare I say, of a tramp - but was well
spoken and very cultured in the arts. He was not a man of any humour and
generally stuck to business. I had no idea he was working with Audacity and am
left curious as to what happened to his phenomenal LP collection. I do hope he
managed to sell when he realised he was ill. He probably was 60 when he died. I
am anyway very sad to hear that he passed away and hope this post will shed
some light on the man.

**Peter Sampson - Audacity Team member 25Aug17**
