# Installing Tenacity and Installation Notes

At the moment, Tenacity only provides binaries for Windows (32 and 64 bit) and
Linux (64 bit only). Unofficial packages also exist for Tenacity, such as the
[Arch Linux tenacity package](https://archlinux.org/packages/extra/x86_64/tenacity/),
the [AUR nightly package](https://aur.archlinux.org/packages/tenacity-git>) and
the [Chocolatey package for Windows](https://community.chocolatey.org/packages/tenacity/1.3.20230123-beta).
Additionally, Tenacity supports other platforms like Haiku R1.

Officially, Windows installers and ZIP archives for both 32 and 64 bit versions
of Windows are provided. For Linux, AppImages are available for some versions
of Tenacity. Starting in 1.3 beta 3, Tenacity is now distributed as a Flatpak
on Linux.

If you are upgrading from a previous version, please read the
[Migration Notes](#migration-notes) for more information and advisories on
upgrading from previous versions.

## System Requirements
Tenacity is neither a heavyweight nor lightweight application. Nevertheless,
your system should have at least some decent specs in order to run Tenacity.

### Hardware

These hardware requirements are somewhat the same across all platforms

- **CPU**: Your CPU should be equiped with SSE2. All modern 64-bit CPUs include
  this feature. Do not worry about this on ARM.

- **RAM**: At least 2 GB available RAM for modest usage. Larger projects require
  more RAM.

- **Storage**: At least 10-15 GB for modest usage, more for larger projects.

- **Display**: 1280x720 is recommended for comfortable usage.

``` {warning}
While we try our best to fix bugs in Tenacity, there are some that
inevitably exist and might take us a while to fix:

- Tenacity's track area is *not* hardware accelerated and might consume an
  undesirable amount of your CPU's resources. We are experimenting with a
  new track panel implementation and hope to add graphics acceleration to
  the track area some time in the future.

- The size of AUP3 projects may increase greatly in size even for a small
  project. If you encounter this, please file a bug report at
  https://codeberg.org/tenacityteam/tenacity/issues. Such behavior is likely
  a bug.
```

### OS

- **Windows**: Windows 7 or higher
- **macOS**: If building from source, macOS 10.15 is the minimum supported
  version.
- **Linux and other OSes**: If there's a Tenacity package available for your
  platform, it will have everything taken care of for you. Optionally install
  FFmpeg for wider import/export support. Otherwise, you should have the
  following dependencies available:

  - wxWidgets 3.1.3 or later **built against GTK 3**. We recommend at least
    3.1.5 for HiDPI support. The latest version is preferred.

  - For MIDI support: PortMidi and PortSMF (see notes for more information).

  - For high-quality stretching: libsbsms 2.1.0 or later.

  - All other dependencies: the latest versions available are recommended.

``` {note}
**On Linux and other OSes**:

- We only support wxGTK with GTK 3 officially; GTK 2 is not supported and
  any GTK 2 specific bugs won't be fixed. The only other wxWidgets port
  we support is wxQt, although that requires patches and is meant for
  advanced users only.

- PortMidi, PortSMF, and libsbsms are not available on all distros, or (for
  libsbsms) your distro might contain the wrong version. For PortSMF, the
  packages might be very old (at least on Debian). Installing the Flatpak
  takes care of all these issues.
```

#### Sound Devices

Most sound devices should work out of the box with Tenacity. If Tenacity is not
detecting our set of speakers, microphone, or other audio input or output
device, try the following:

- Try a different host. Click on first dropdown menu of the device toolbar and
  select a different host. Then check to see if the device is there by checking
  either the input or output devices.

- Make sure you have the proper drivers installed. Visit the manufacturer's
  website for more info.

- Test with another application. Does it show up in your system's settings? Does
  a media player or other application see the device? If so, this might be a
  bug or a limitation of Tenacity.

## Installation on Windows

To install on Windows, simply download one of the installers and run them.
Follow the on-screen instructions to install Tenacity. This also works for
upgrading Tenacity from prior versions.

## Installation on Linux

### Flatpak

Tenacity is available on Flathub. First, add it if you haven't already:

```bash
$ flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
```

To install Tenacity, run the following:

```bash
$ flatpak install org.tenacityaudio.Tenacity
```

Alternatively, you may use a GUI frontend like GNOME Software or KDE Discover.
Once Flathub is enabled, search "tenacity", and it should be the first result.
If you don't see the result, you might need to enable Flathub first.

### AppImage

Some versions of Tenacity are available in the Appimage format. You may choose
to use these AppImages for convenience, but they are provided as-is and will
not receive support for any AppImage-specific issues. Any issues with Tenacity
itself will still be supported.

There is no need to install an AppImage. However, the closest way to
"installing" the Tenacity AppImage on your system is by placing it in a
directory (e.g., `/opt`) and adding that directory to your `$PATH`. For
example:

``` bash
$ cp tenacity.AppImage /opt/bin/tenacity # Copy the Tenacity AppImage to /opt/bin
$ nano ~/.bashrc                         # Edit your .bashrc to add /opt/bin to your $PATH
$ tenacity                               # Run Tenacity
```

(Note that the following does NOT add a `.desktop` entry; Tenacity will not
show up in desktop environments).

## Advanced: Building

Alternatively, you can build Tenacity yourself on any supported platform.
Build instructions can be found [here](https://codeberg.org/tenacityteam/tenacity/src/main/BUILDING.md).

``` {note}
Building is meant for more advanced users and requires good CPU, RAM, and
storage resources.
```
### Building an AppImage

It is also possible to build an AppImage. After building Tenacity, set the
environment variable `VCPKG_LIB_PATH` to the path vcpkg installed libraries to
(usually in the root of the build folder under
`vcpkg_installed/<triplet>/lib`). If you did not use vcpkg while building
Tenacity, set it to an empty string (i.e., `export VCPKG_LIB_PATH=""`). Then
run `cpack` at the root of the build folder. If everything succeeds, the
AppImage will be under `package/` as
`tenacity-linux-<tag>-XXXXX-gXXXXXX.AppImage`.

(migration-notes)=

## Migrating from Previous Versions

### Upgrading From Betas

Upgrading from beta releases is usually not supported, but due to the special
circumstances regarding the 1.3 betas, upgrading from those versions to stable
releases will be supported for now.

### Builtin Effects Are Disabled

It is a known issue that upgrading from any of the 1.3 betas to a stable
version causes you to loose access to all plugins. This is because some
information about the builtin effects was changed, and due to the way
Tenacity's plugin manager works, it fails "loading" builtin effects.

The solution is quite simple: delete the old plugin registry
(pluginregistry.cfg) from your settings folder. If you are on Windows, delete
`C:\Users<username>\AppData\Roaming\Tenacity\pluginregistry.cfg`. If you are
on Linux, there are two different files you need to delete depending on if you
use the Flatpak or not:

- Delete `~/.var/app/org.tenacityaudio.Tenacity/config/pluginregistry.cfg` if
  **DO** use Flatpak.
- Delete `~/.config/tenacity/pluginregistry.cfg` if you **DON'T** use Flatpak.

``` {note}
For your convenience, Tenacity shows the location of your settings folder
under *Help > About Tenacity... > Build Information*. If the location of
your settings folder differs from the ones mentioned above, use this
location.
```
