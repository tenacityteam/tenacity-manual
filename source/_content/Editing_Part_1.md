# Editing Part 1: The Concepts
Now that you got the basics down, let's introduce you to all of what Tenacity
has to offer. To do that, we need to introduce some of the fundamentals
concepts that make up the core featurs of Tenacity.

## Clips
One of the fundamentals in Tenacity is a clip. A clip is a region of data,
whether that's audio data, MIDI data, or label data, that spans a certain range
in the timeline.

## Track
A track is a collection of clips. You can add, move, or delete clips in a
track.

There are four different types of tracks in Tenacity:

* Audio tracks
* MIDI tracks
* Label tracks
* Time tracks

Time tracks are a bit special. See the appropriate section below.

Each track, spare time tracks, contains a clip type. For audio tracks, this is
an audio clip. For MIDI tracks, this is a MIDI clip. Label tracks do have a
"label clip", but they are called "labels" for simplicity. Time tracks,
however, are a bit different. See the section about time tracks for more
details.

**Fun fact**: In Tenacity's source code, you'll see an audio track referred to
as a `WaveTrack`, a MIDI track as a `NoteTrack`, a label track as a
`LabelTrack`, and a time track as a `TimeTrack`.

### Audio Tracks and MIDI Tracks

Audio and MIDI tracks can be muted, soloed, and panned, as well as have their
gain adjusted (for MIDI tracks, this is known as "velocity"). Label tracks, on
the other hand, do not have any controls.

Admittedly, Tenacity does not include good support for MIDI (yet!). You can
only import MIDI, manage a single MIDI clip per track at a time, and play back
MIDI (which requires some setup). In future versions, we are looking at
bringing MIDI editing to par with audio editing, and later adding a proper MIDI
editor so you can edit audio alongside MIDI and vice versa.

### Label Tracks

In Audacity, label tracks served the purposes of allowing portions of the
timeline to be labeled. In Tenacity this purpose has expanded to modifying
chapters, cue points, or similar marks in select audio formats. For example,
if a PCM-encoded or FLAC-encoded Matroska (MKA) file containing chapters is
imported in Tenacity, you have the option to import them as label tracks.
Similarly, you have the option of exporting any label tracks as chapters when
exporting to a Matroska file.

### Time Tracks

Time tracks are the "special" track because unlike the others, you can only
have one time track per project. Time tracks contain time points, which only
consist of one time point in the timeline, instead of clips. The purpose of a
time track is different than the other tracks, and that is to manipulate the
project's speed.

## Timeline
Perhaps the basics of any audio editor or DAW is the timeline. The timeline
contains all your project's clips, labels, or time points. (For more
information on what those are, see below).

## The Track Area
The track area is the area that contains all your tracks in the project. You
can add new tracks by right clicking any empty space in the track area or
rearrange tracks in the project timeline by dragging a track above or below
another.

## Selections
When a region of the timeline is selected, whether across a single track or
multiple track, that selected region is known as a selection. You can only have
one selection at a time.

In case you want to select all audio when nothing is selectedfor an operation
that requires a selection, you can enable a preference option.


