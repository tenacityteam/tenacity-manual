# The Tenacity Manual

```{toctree}
:maxdepth: 3
:caption: Table Of Contents:

_content/Quick_Help
_content/Introduction_and_Motivation
_content/Installing_and_Installation_Notes
_content/Basics
_content/Editing_Part_1
_content/Editing_Part_2
_content/Importing_and_Exporting
_content/FFmpeg
_content/Effects_Plugins_and_Modules
_content/Preferences
_content/Audacity_Tenacity_Differences
_content/ASIO
_content/Scripting
_content/Tips_And_Tricks
_content/Acknowledgements
```

## Welcome!

Welcome to the Tenacity Manual! This manual details everything about Tenacity,
from general concepts to the very technical details of how something works.

Tenacity is a free and open source audio editor forked from the popular audio
editor Audacity. Tenacity is licensed under the GNU General Public License
version 2 or later. It contains the following features:

- Recording from audio devices, real or virtual.
- Support for importing from and exporting to a wide range of audio formats, extensible with FFmpeg.
- High quality audio processing, including up to 32-bit float audio support.
- Extensibility with support for plug-ins, providing support for VST 2, LADSPA/LV2, and AU plugins.
- Support for scripting in the built-in scripting language Nyquist, or in Python, Perl, and other languages with named pipes.
- Editing arbitrary sampling and multi-track timeline.
- Accessibility including editing via keyboard, screen reader support and narration support.
- Tools useful in the analysis of signals, including audio.

Tenacity does not have a single application when it comes to audio processing.
Musicians recording real instruments can make a song in Tenacity, podcasters
can edit their episodes in Tenacity, and even academic researchers can analyze
signals using Tenacity. We don't intend to focus Tenacity on a *single* area
of audio processing, but rather, we intend to focus Tenacity over audio
processing *in general*, including the aforementioned applications. Therefore,
we intend to focus this manual on all possible use cases and applications of
Tenacity.

## Getting the Correct Version of the Manual

Releases of the manual are made for each minor branch of Tenacity covering each
version in that branch. For example, the 1.3.x branch of the manual covers
documentation for versions 1.3, 1.3.1, etc. If the manual refers to a version
that hasn't been released yet, it is a development manual.

This version of the manual covers Tenacity 1.4.x. If you have a different
version, check out https://codeberg.org/tenacityteam/tenacity-manual/releases
to find the right manual version.

Alternatively, you can visit https://tenacityaudio.org/docs/ for the latest
version of the manual. Keep in mind that we don't host older versions of the
manual online, so you'll need to manually install those versions to view them.

## Reporting Bugs, Typos, and Corrections

If you found a typo, incorrect documentation, or otherwise want to suggest an
idea, go to https://codeberg.org/tenacityteam/tenacity-manual/issues to file an
issue report.

If you want to make a change, fork the manual repository on Codeberg and open
a pull request. Your contribution is appreciated!

## License

This manual is licensed under [Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by/4.0/).
You are welcome to contribute to the manual in order to improve it so long as
your contributions are made available under this same license.

Tenacity is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
Suite 330, Boston, MA 02111-1307 USA.
