# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2022-2024 Tenacity Community. Licensed under CC-BY 4.0
# This file is distributed under the same license as the Tenacity package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Tenacity Manual 1.4\n"
"Report-Msgid-Bugs-To: ~tenacity/tenacity-discuss@lists.sr.ht\n"
"POT-Creation-Date: 2024-04-01 08:35-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/index.rst:10
msgid "Table Of Contents:"
msgstr ""

#: ../../source/index.rst:8
msgid "The Tenacity Manual"
msgstr ""

#: ../../source/index.rst:28
msgid "Welcome!"
msgstr ""

#: ../../source/index.rst:30
msgid "Welcome to the Tenacity Manual! This manual details everything about Tenacity, from general concepts to the very technical details of how something works."
msgstr ""

#: ../../source/index.rst:33
msgid "Tenacity is a free and open source audio editor forked from the popular audio editor Audacity. Tenacity is licensed under the GNU General Public License version 2 or later. It contains the following features:"
msgstr ""

#: ../../source/index.rst:37
msgid "Recording from audio devices, real or virtual."
msgstr ""

#: ../../source/index.rst:38
msgid "Support for importing from and exporting to a wide range of audio formats, extensible with FFmpeg."
msgstr ""

#: ../../source/index.rst:39
msgid "High quality audio processing, including up to 32-bit float audio support."
msgstr ""

#: ../../source/index.rst:40
msgid "Extensibility with support for plug-ins, providing support for VST 2, LADSPA/LV2, and AU plugins."
msgstr ""

#: ../../source/index.rst:41
msgid "Support for scripting in the built-in scripting language Nyquist, or in Python, Perl, and other languages with named pipes."
msgstr ""

#: ../../source/index.rst:42
msgid "Editing arbitrary sampling and multi-track timeline."
msgstr ""

#: ../../source/index.rst:43
msgid "Accessibility including editing via keyboard, screen reader support and narration support."
msgstr ""

#: ../../source/index.rst:44
msgid "Tools useful in the analysis of signals, including audio."
msgstr ""

#: ../../source/index.rst:46
msgid "Tenacity does not have a single application when it comes to audio processing. Musicians recording real instruments can make a song in Tenacity, podcasters can edit their episodes in Tenacity, and even academic researchers can analyze signals using Tenacity. We don't intend to focus Tenacity on a *single* area of audio processing, but rather, we intend to focus Tenacity over audio processing *in general*, including the aforementioned applications. Therefore, we intend to focus this manual on all possible use cases and applications of Tenacity."
msgstr ""

#: ../../source/index.rst:56
msgid "Getting the Correct Version of the Manual"
msgstr ""

#: ../../source/index.rst:58
msgid "Releases of the manual are made for each minor branch of Tenacity covering each version in that branch. For example, the 1.3.x branch of the manual covers documentation for versions 1.3, 1.3.1, etc. If the manual refers to a version that hasn't been released yet, it is a development manual."
msgstr ""

#: ../../source/index.rst:63
msgid "This version of the manual covers Tenacity 1.4.x. If you have a different version, check out https://codeberg.org/tenacityteam/tenacity-manual/releases to find the right manual version."
msgstr ""

#: ../../source/index.rst:67
msgid "Alternatively, you can visit https://tenacityaudio.org/docs/ for the latest version of the manual. Keep in mind that we don't host older versions of the manual online, so you'll need to manually install those versions to view them."
msgstr ""

#: ../../source/index.rst:72
msgid "Reporting Bugs, Typos, and Corrections"
msgstr ""

#: ../../source/index.rst:74
msgid "If you found a typo, incorrect documentation, or otherwise want to suggest an idea, go to https://codeberg.org/tenacityteam/tenacity-manual/issues to file an issue report."
msgstr ""

#: ../../source/index.rst:78
msgid "If you want to make a change, fork the manual repository on Codeberg and open a pull request. Your contribution is appreciated!"
msgstr ""

#: ../../source/index.rst:82
msgid "License"
msgstr ""

#: ../../source/index.rst:84
msgid "This manual is licensed under `Creative Commons Attribution 4.0 <https://creativecommons.org/licenses/by/4.0/>`_. You are welcome to contribute to the manual in order to improve it so long as your contributions are made available under this same license."
msgstr ""

#: ../../source/index.rst:89
msgid "Tenacity is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version."
msgstr ""

#: ../../source/index.rst:94
msgid "This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details."
msgstr ""

#: ../../source/index.rst:98
msgid "You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA."
msgstr ""
