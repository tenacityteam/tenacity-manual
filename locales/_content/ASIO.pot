# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2022-2024 Tenacity Community. Licensed under CC-BY 4.0
# This file is distributed under the same license as the Tenacity package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Tenacity \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-06-30 07:51-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/_content/ASIO.rst:3
msgid "ASIO in Tenacity"
msgstr ""

#: ../../source/_content/ASIO.rst:5
msgid "Currently, Tenacity does not officially support ASIO directly. There is also no way to build Tenacity with ASIO support."
msgstr ""

#: ../../source/_content/ASIO.rst:8
msgid "Tenacity's ASIO support depends on PortAudio, the library Tenacity uses for recording and playback. As of 1.3, there is no way to automatically build a copy of PortAudio with ASIO enabled."
msgstr ""

#: ../../source/_content/ASIO.rst:13
msgid "There might be a way to automatically build an ASIO-enabled PortAudio via vcpkg. If you find a way to do this, please let us know."
msgstr ""

#: ../../source/_content/ASIO.rst:16
msgid "Despite the lack of any offical build options, it is possible to replace the stock ``portaudio.dll`` with a custom ASIO-enabled one. Keep in mind that a) this is for advanced users only; b) you cannot distribute ASIO-enabled Tenacity builds; and c) this is unsupported. If you are still encouraged to build your own version of PortAudio for private use with Tenacity, you can follow these steps:"
msgstr ""

#: ../../source/_content/ASIO.rst:23
msgid "`Build PortAudio with ASIO support <https://portaudio.com/docs/v19-doxydocs/compile_windows_asio_msvc.html>`_. It is assumed that you know how to set up the build environment. If not, you will need to learn how to set up a proper build environment. **We will not accept support requests for building PortAudio.**"
msgstr ""

#: ../../source/_content/ASIO.rst:28
msgid "In an existing Tenacity installation, rename ``portaudio.dll`` to ``portaudio.dll.bak``."
msgstr ""

#: ../../source/_content/ASIO.rst:31
msgid "Copy the custom ``portaudio.dll`` to the build folder."
msgstr ""

#: ../../source/_content/ASIO.rst:34
msgid "For step 1, it is currently recommended to build the latest git version of PortAudio if you need to record desktop audio. Once PortAudio 19.8 is released, it is recommended you use that version instead."
msgstr ""

#: ../../source/_content/ASIO.rst:38
msgid "Also, whenever you update Tenacity, the installer will overwrite your custom portaudio.dll. You will need to repeat steps 2 and 3 again in order to restore ASIO support."
msgstr ""

#: ../../source/_content/ASIO.rst:43
msgid "Distributing Builds"
msgstr ""

#: ../../source/_content/ASIO.rst:46
msgid "ASIO is a proprietary protocol. We cannot legally distribute builds with ASIO support enabled. Requests for ASIO-enabled builds will be ignored and closed as wontfix."
msgstr ""

#: ../../source/_content/ASIO.rst:50
msgid "**You cannot distribute custom ASIO-enabled builds either. It is still illegal for the same reasons we cannot distribute such builds.**"
msgstr ""
